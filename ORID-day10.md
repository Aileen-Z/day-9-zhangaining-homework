## 1、Objective
Flyway : I learned how to configure and use Flyway, but I need to learn exactly how to do version control with it.\
	
Spring Boot Mapper : Learned how to map request and response data in Spring Boot, so that the request data is mapped and then added to the database.\
	
Cloud Native : It is divided into four parts: microservices, DevOPs, continuous delivery, and containerization. Microservices are applications that communicate with each other through RESTful APIs and can be deployed, updated, scaled, and restarted independently. Continuous delivery refers to frequent releases, rapid delivery, rapid feedback, and reduced release risk. Containerization is the best carrier for microservices.

## 2、Reflective
useful

## 3、Interpretive
I found that when I needed to know something unfamiliar but didn't have enough time, I could quickly grasp the general use of the framework by doing the same assignment over and over again.\
	
I will express and communicate more and let the mentor and team know my situation in time.

## 4、Decisional
None

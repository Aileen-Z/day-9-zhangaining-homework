package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.apache.catalina.mapper.Mapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final EmployeeJPARepository employeeJPARepository;

    private final CompanyJPARepository companyJPARepository;

    public CompanyService(EmployeeJPARepository employeeJPARepository, CompanyJPARepository companyJPARepository) {
        this.employeeJPARepository = employeeJPARepository;
        this.companyJPARepository = companyJPARepository;
    }

    public List<Company> findAll() {
        return companyJPARepository.findAll();
    }


    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        List<Company> com = companyJPARepository.findAll(pageRequest).getContent();
        return com.stream()
                .map(CompanyMapper::toResponse)
                .collect(Collectors.toList());
    }

    public CompanyResponse findById(Long id) {
        Company company = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = employeeJPARepository.findByCompanyId(company.getId());
        company.setEmployees(employees);
        CompanyResponse companyResponse = CompanyMapper.toResponse(company);
        companyResponse.setEmployeesCount(company.getEmployees().size());
        return companyResponse;
    }

    public CompanyResponse update(Long id, CompanyRequest request) {
        Company updateCompany = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        if (request.getName() != null) {
            updateCompany.setName(request.getName());
        }
        updateCompany = companyJPARepository.save(updateCompany);
        CompanyResponse companyResponse = CompanyMapper.toResponse(updateCompany);
        companyResponse.setEmployeesCount(employeeJPARepository.findByCompanyId(id).size());
        return companyResponse;
    }

    public CompanyResponse create(Company company) {
        Company save = companyJPARepository.save(company);
        return CompanyMapper.toResponse(save);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeJPARepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}

package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {


    private final EmployeeJPARepository employeeJPARepository;
    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;

        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return employeeJPARepository.findAll().stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        Employee targetEmployee = employeeJPARepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(targetEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeJPARepository.findByGender(gender);
    }

    public EmployeeResponse create(Employee employee) {
        Employee save = employeeJPARepository.save(employee);
        return EmployeeMapper.toResponse(save);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return employeeJPARepository.findAll(pageRequest).getContent();
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }


    public EmployeeResponse update(Long id, Employee toEntity) {
        Employee toBeUpdatedEmployee = employeeJPARepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        if(toEntity.getSalary() != null){
            toBeUpdatedEmployee.setSalary(toEntity.getSalary());
        }
        if(toEntity.getAge() != null){
            toBeUpdatedEmployee.setAge(toEntity.getAge());
        }
        Employee save = employeeJPARepository.save(toBeUpdatedEmployee);
        return EmployeeMapper.toResponse(save);
    }
}

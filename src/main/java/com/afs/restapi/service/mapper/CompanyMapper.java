package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public CompanyMapper(){}
    public static Company toEntity(CompanyRequest request){
        Company company = new Company();
        BeanUtils.copyProperties(request, company);
        return company;
    }
    public static CompanyResponse toResponse(Company company){
        CompanyResponse response = new CompanyResponse();
        BeanUtils.copyProperties(company, response);
        return response;
    }
}

## 1、Objective
We learned about SQL, Spring Data JPA today and practiced rewriting test files with JPA and modifying the corresponding implementation code. Here are some summaries of knowledge points and tips on how to use them.

1.SQL:
Some basic statements and usage of SQL.

2.Spring Data JPA:
	
Full name of the Java Persistence API, can be annotated or XML description of the "object - relational table" between the mapping relationship , and the entity object persistence to the database .	
You can put a yml file in both the test directory and the main directory, with different configurations, to avoid situations where the test makes changes to the real data base.

## 2、Reflective
useful

## 3、Interpretive
I will learn more about spring boot.

## 4、Decisional
studying and refining yesterday's assignment further.
